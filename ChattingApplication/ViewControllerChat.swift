//
//  ViewControllerChat.swift
//  ChattingApplication
//
//  Created by Sierra 4 on 21/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit
import ASJExpandableTextView
class ViewControllerChat: UIViewController {
    var message_data_array = [String]()
    var sender_id_array = [Int]()
    var sender_id = Int()
    var receiver_id : Int = 201
    @IBOutlet weak var segPerson: UISegmentedControl!
    @IBOutlet weak var txtMessage: ASJExpandableTextView!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgProfle: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    var Id = Int()
    var name = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Id)
        sender_id = Id
    //    DeleteAllData()
        lblName.text = name
         tableView.estimatedRowHeight = 60
         tableView.rowHeight = UITableViewAutomaticDimension
        retrieveMessages()
    }
    @IBAction func btnBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func segPerson(_ sender: UISegmentedControl) {
        personSelector(obj : sender)
    }
    @IBAction func btnSend(_ sender: UIButton) {
        if(txtMessage.text != ""){
            message_data_array.append(((txtMessage.text)?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!)
            saveMessages(txtMessage.text!)
            tableView.reloadData()
            txtMessage.text = ""
        }
        else{
            print("nthng")
        }
    }
}
