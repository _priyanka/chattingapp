//
//  Senders.swift
//  ChattingApplication
//
//  Created by Sierra 4 on 21/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import Foundation
import CoreData
import  UIKit

extension ViewController
{
/*
 func DeleteAllData(){
 let appDelegate = UIApplication.shared.delegate as! AppDelegate
 let managedContext = appDelegate.persistentContainer.viewContext
 let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "Sender"))
 do {
 try managedContext.execute(DelAllReqVar)
 print("Delete Successfully")
 }
 catch {
 print(error)
 }
 }
 */
    func saveSenders()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        //code to save the data
        let senders = NSEntityDescription.insertNewObject(forEntityName: "Sender", into: context)
        
        senders.setValue("sapna", forKey: "sender_name")
        senders.setValue(103, forKey: "sender_id")
        do
        {
            try context.save()
            print("SAVED")
        }
        catch
        {
            print("There is some error ")
        }
    }
    
    func retrieveSenders()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let senders = NSFetchRequest<NSFetchRequestResult>(entityName: "Sender")
        
        let context = appDelegate.persistentContainer.viewContext
        senders.returnsObjectsAsFaults = false
        do
        {
            let results = try context.fetch(senders)
            print("RESULTS",results.count)
            if results.count > 0
            {
                for result in results as! [NSManagedObject]
                {
                    if let sender_name = result.value(forKey: "sender_name") as? String
                    {
                        arrayName.append(sender_name)
                        print(sender_name)
                        
                    }
                    if let sender_id = result.value(forKey: "sender_id") as? Int
                    {
                        arrayId.append(sender_id)
                        print(sender_id)
                    }
                }
                print(arrayName)
                //   print(arrayId)
            }
        }
        catch
        {
            print("Error while retireving a data ")
        }
    }
}
