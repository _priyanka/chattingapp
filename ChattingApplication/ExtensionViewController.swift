//
//  ExtensionViewController.swift
//  ChattingApplication
//
//  Created by Sierra 4 on 21/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension ViewControllerChat:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return message_data_array.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if(sender_id_array[indexPath.row] == 101){
        guard let cell:TableViewCellChatReciever = tableView.dequeueReusableCell(withIdentifier: "identifierReciever", for: indexPath) as? TableViewCellChatReciever else{ return TableViewCellChatReciever()}
        cell.lblMesaage.text = message_data_array[indexPath.row]
        return cell
        }
        else {
        guard let cell:TableViewCellChatSender = tableView.dequeueReusableCell(withIdentifier: "identifierSender", for: indexPath) as? TableViewCellChatSender else{ return TableViewCellChatSender()}
        cell.lblMesaage.text = message_data_array[indexPath.row]
        return cell
        }
    }
    func personSelector(obj : UISegmentedControl){
        if(obj.selectedSegmentIndex == 0)
        {
            sender_id = Id
            receiver_id = 201
        }
        else{
            sender_id = 201
            receiver_id = Id
        }
    }
    
    func DeleteAllData(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "Messages"))
        do {
            try managedContext.execute(DelAllReqVar)
            print("Delete Successfully")
        }
        catch {
            print(error)
        }
    }
    
    func saveMessages(_ mymsg:String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        //code to save the data
        let messages = NSEntityDescription.insertNewObject(forEntityName: "Messages", into: context)
        messages.setValue(1, forKey: "message_id")
        messages.setValue(mymsg, forKey: "message_data")
        messages.setValue(sender_id,forKey: "sender_id")
        messages.setValue(receiver_id,forKey: "receiver_id")
        messages.setValue("text", forKey: "message_type")
        do
        {
            try context.save()
            print("SAVED")
        }
        catch
        {
            print("There is some error ")
        }
    }
    
    func retrieveMessages()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let messages = NSFetchRequest<NSFetchRequestResult>(entityName: "Messages")
        
        let context = appDelegate.persistentContainer.viewContext
        messages.returnsObjectsAsFaults = false
        do
        {
            let results = try context.fetch(messages)
            print("RESULTS",results.count)
            if results.count > 0
            {
                for result in results as! [NSManagedObject]
                {
                    let message = result.value(forKey: "message_data") as? String
                    let fetch_sid = result.value(forKey: "sender_id") as? Int
                    let fetch_rid = result.value(forKey: "receiver_id") as? Int
                    
                    if ((sender_id == fetch_sid &&  receiver_id == fetch_rid)
                        || (sender_id == fetch_rid &&  receiver_id == fetch_sid))
                    {
                        sender_id_array.append(fetch_sid!)
                        message_data_array.append(message!)
                        print(fetch_sid ?? 0)
                        print("message",message ?? " ")
                    }
                }
            }
            print(sender_id_array)
        }
        catch
        {
            print("Error while retireving a data ")
        }
    }
   
    }

